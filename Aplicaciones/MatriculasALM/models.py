from django.db import models

# Create your models here.

class Carrera(models.Model):
    idCarrera_alm=models.AutoField(primary_key=True)
    nombreCarrera_alm=models.CharField(max_length=50)
    directorCarrera_alm=models.CharField(max_length=50)
    extencionCarrera_alm=models.CharField(max_length=50)
    duracionCarrera_alm=models.CharField(max_length=50)
    logoCarrera_alm=models.FileField(upload_to='carreras', null=True,blank=True)


class Curso(models.Model):
    idCurso_alm=models.AutoField(primary_key=True)
    nivelCurso_alm=models.CharField(max_length=50)
    descripcionCurso_alm=models.CharField(max_length=50)
    aulaCurso_alm=models.CharField(max_length=50)
    horarioCurso_alm=models.FileField(upload_to='pdfCurso/')
    profesorCurso_alm=models.CharField(max_length=50)
    carrera=models.ForeignKey(Carrera,null=True,blank=True,on_delete=models.CASCADE)

class Asignatura(models.Model):
    idAsignatura_alm=models.AutoField(primary_key=True)
    nombreAsignatura_alm=models.CharField(max_length=50)
    creditosAsignatura_alm=models.CharField(max_length=50)
    fechaInicioAsignatura_alm=models.DateField()
    fechaFinalizacionAsignatura_alm=models.DateField()
    profesorAsignatura_alm=models.CharField(max_length=50)
    silaboAsignatura_alm=models.FileField(upload_to='pdfAsignatura/')
    horarioAsignatura_alm=models.CharField(max_length=50)
    descripcionAsignatura_alm=models.TextField()
    curso=models.ForeignKey(Curso,null=True,blank=True,on_delete=models.CASCADE)
