from django.urls import path
from . import views

urlpatterns=[
    path('', views.index, name='index'),

    path('carrera/', views.listadoCarreras, name='carrera'),
    path('guardarCarrera/', views.guardarCarrera),
    path('eliminarCarrera/<idCarrera_alm>', views.eliminarCarrera),
    path('editarCarrera/<idCarrera_alm>', views.editarCarrera),
    path('procesarActualizacionCarrera/', views.procesarActualizacionCarrera),

    path('curso/', views.listadoCursos, name='curso'),
    path('guardarCurso/', views.guardarCurso),
    path('eliminarCurso/<idCurso_alm>', views.eliminarCurso),
    path('editarCurso/<idCurso_alm>', views.editarCurso),
    path('procesarActualizacionCurso/', views.procesarActualizacionCurso),

    path('asignatura/', views.listadoAsignaturas, name='asignatura'),
    path('guardarAsignatura/', views.guardarAsignatura),
    path('eliminarAsignatura/<idAsignatura_alm>', views.eliminarAsignatura),
    path('editarAsignatura/<idAsignatura_alm>', views.editarAsignatura),
    path('procesarActualizacionAsignatura/', views.procesarActualizacionAsignatura),
]
