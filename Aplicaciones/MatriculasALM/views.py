from django.shortcuts import render, redirect
from .models import *
from django.contrib import messages

# Create your views here.

def index(request):
    return render(request,'index.html')

def listadoCarreras(request):
    carrerasBdd=Carrera.objects.all()
    return render(request,'carrera.html',{'carreras':carrerasBdd})

def guardarCarrera(request):
    nombreCarrera_alm=request.POST["nombreCarrera_alm"]
    directorCarrera_alm=request.POST["directorCarrera_alm"]
    extencionCarrera_alm=request.POST["extencionCarrera_alm"]
    duracionCarrera_alm=request.POST["duracionCarrera_alm"]
    logoCarrera_alm=request.FILES.get("logoCarrera_alm")

    nuevoCarrera=Carrera.objects.create(nombreCarrera_alm=nombreCarrera_alm, directorCarrera_alm=directorCarrera_alm,
     extencionCarrera_alm=extencionCarrera_alm, duracionCarrera_alm=duracionCarrera_alm, logoCarrera_alm=logoCarrera_alm)

    messages.success(request, 'Carrera Guardado exitosamente')
    return redirect('/carrera')

def eliminarCarrera(request,idCarrera_alm):
    carreraEliminar=Carrera.objects.get(idCarrera_alm=idCarrera_alm)
    carreraEliminar.delete()
    messages.success(request, 'Carrera Eliminado exitosamente')
    return redirect('/carrera')

def editarCarrera(request,idCarrera_alm):
    carreraEditar=Carrera.objects.get(idCarrera_alm=idCarrera_alm)
    return render(request,
    'editarCarrera.html',{'carrera':carreraEditar})

def procesarActualizacionCarrera(request):
    idCarrera_alm=request.POST["idCarrera_alm"]
    nombreCarrera_alm=request.POST["nombreCarrera_alm"]
    directorCarrera_alm=request.POST["directorCarrera_alm"]
    extencionCarrera_alm=request.POST["extencionCarrera_alm"]
    duracionCarrera_alm=request.POST["duracionCarrera_alm"]
    logoCarrera_alm=request.FILES.get("logoCarrera_alm")
    carreraEditar=Carrera.objects.get(idCarrera_alm=idCarrera_alm)
    carreraEditar.nombreCarrera_alm=nombreCarrera_alm
    carreraEditar.directorCarrera_alm=directorCarrera_alm
    carreraEditar.extencionCarrera_alm=extencionCarrera_alm
    carreraEditar.duracionCarrera_alm=duracionCarrera_alm
    if logoCarrera_alm is not None:
        carreraEditar.logoCarrera_alm=logoCarrera_alm
    carreraEditar.save()
    messages.success(request,'Carrera ACTUALIZADO Exitosamente')
    return redirect('/carrera')

def listadoCursos(request):
    cursosBdd=Curso.objects.all()
    carrerasBdd=Carrera.objects.all()
    return render(request,'curso.html',{'cursos':cursosBdd, 'carreras':carrerasBdd, 'navbar': 'curso'})

def guardarCurso(request):
    id_car=request.POST["id_car"]
    carreraSeleccionado=Carrera.objects.get(idCarrera_alm=id_car)
    nivelCurso_alm=request.POST["nivelCurso_alm"]
    descripcionCurso_alm=request.POST["descripcionCurso_alm"]
    aulaCurso_alm=request.POST["aulaCurso_alm"]
    horarioCurso_alm=request.FILES.get("horarioCurso_alm")
    profesorCurso_alm=request.POST["profesorCurso_alm"]


    nuevoCurso=Curso.objects.create(nivelCurso_alm=nivelCurso_alm, descripcionCurso_alm=descripcionCurso_alm, aulaCurso_alm=aulaCurso_alm,
    horarioCurso_alm=horarioCurso_alm, profesorCurso_alm=profesorCurso_alm, carrera=carreraSeleccionado)

    messages.success(request, 'Curso Guardado exitosamente')
    return redirect('/curso')

def eliminarCurso(request,idCurso_alm):
    cursoEliminar=Curso.objects.get(idCurso_alm=idCurso_alm)
    cursoEliminar.delete()
    messages.success(request, 'Curso Eliminado exitosamente')
    return redirect('/curso')

def editarCurso(request,idCurso_alm):
    cursoEditar=Curso.objects.get(idCurso_alm=idCurso_alm)
    carrerasBdd=Carrera.objects.all()
    return render(request,
    'editarCurso.html',{'curso':cursoEditar, 'carreras':carrerasBdd})


def procesarActualizacionCurso(request):
    idCurso_alm=request.POST["idCurso_alm"]
    id_car=request.POST["id_car"]
    carreraSeleccionado=Carrera.objects.get(idCarrera_alm=id_car)
    nivelCurso_alm=request.POST["nivelCurso_alm"]
    descripcionCurso_alm=request.POST["descripcionCurso_alm"]
    aulaCurso_alm=request.POST["aulaCurso_alm"]
    horarioCurso_alm=request.FILES.get("horarioCurso_alm")
    profesorCurso_alm=request.POST["profesorCurso_alm"]

    #Insertando datos mediante el ORM de DJANGO
    cursoEditar=Curso.objects.get(idCurso_alm=idCurso_alm)
    cursoEditar.carrera=carreraSeleccionado
    cursoEditar.nivelCurso_alm=nivelCurso_alm
    cursoEditar.descripcionCurso_alm=descripcionCurso_alm
    cursoEditar.aulaCurso_alm=aulaCurso_alm
    if horarioCurso_alm is not None:
        cursoEditar.horarioCurso_alm=horarioCurso_alm

    cursoEditar.profesorCurso_alm=profesorCurso_alm

    cursoEditar.save()
    messages.success(request,
      'Curso ACTUALIZADO Exitosamente')
    return redirect('/curso')

def listadoAsignaturas(request):
    asignaturasBdd=Asignatura.objects.all()
    cursosBdd=Curso.objects.all()
    return render(request,'asignatura.html',{'asignaturas':asignaturasBdd, 'cursos':cursosBdd, 'navbar': 'asignatura'})

def guardarAsignatura(request):
    id_cur=request.POST["id_cur"]
    cursoSeleccionado=Curso.objects.get(idCurso_alm=id_cur)
    nombreAsignatura_alm=request.POST["nombreAsignatura_alm"]
    creditosAsignatura_alm=request.POST["creditosAsignatura_alm"]
    fechaInicioAsignatura_alm=request.POST["fechaInicioAsignatura_alm"]
    fechaFinalizacionAsignatura_alm=request.POST["fechaFinalizacionAsignatura_alm"]
    profesorAsignatura_alm=request.POST["profesorAsignatura_alm"]
    silaboAsignatura_alm=request.FILES.get("silaboAsignatura_alm")
    horarioAsignatura_alm=request.POST["horarioAsignatura_alm"]
    descripcionAsignatura_alm=request.POST["descripcionAsignatura_alm"]


    nuevoAsignatura=Asignatura.objects.create(nombreAsignatura_alm=nombreAsignatura_alm, creditosAsignatura_alm=creditosAsignatura_alm, fechaInicioAsignatura_alm=fechaInicioAsignatura_alm,
    fechaFinalizacionAsignatura_alm=fechaFinalizacionAsignatura_alm, profesorAsignatura_alm=profesorAsignatura_alm, silaboAsignatura_alm=silaboAsignatura_alm,
    horarioAsignatura_alm=horarioAsignatura_alm, descripcionAsignatura_alm=descripcionAsignatura_alm, curso=cursoSeleccionado)

    messages.success(request, 'Asignatura Guardado exitosamente')
    return redirect('/asignatura')

def eliminarAsignatura(request,idAsignatura_alm):
    asignaturaEliminar=Asignatura.objects.get(idAsignatura_alm=idAsignatura_alm)
    asignaturaEliminar.delete()
    messages.success(request, 'Asignatura Eliminado exitosamente')
    return redirect('/asignatura')

def editarAsignatura(request,idAsignatura_alm):
    asignaturaEditar=Asignatura.objects.get(idAsignatura_alm=idAsignatura_alm)
    cursosBdd=Curso.objects.all()
    return render(request,
    'editarAsignatura.html',{'asignatura':asignaturaEditar, 'cursos':cursosBdd})

def procesarActualizacionAsignatura(request):
    idAsignatura_alm=request.POST["idAsignatura_alm"]
    id_cur=request.POST["id_cur"]
    cursoSeleccionado=Curso.objects.get(idCurso_alm=id_cur)
    nombreAsignatura_alm=request.POST["nombreAsignatura_alm"]
    creditosAsignatura_alm=request.POST["creditosAsignatura_alm"]
    fechaInicioAsignatura_alm=request.POST["fechaInicioAsignatura_alm"]
    fechaFinalizacionAsignatura_alm=request.POST["fechaFinalizacionAsignatura_alm"]
    profesorAsignatura_alm=request.POST["profesorAsignatura_alm"]
    silaboAsignatura_alm=request.FILES.get("silaboAsignatura_alm")
    horarioAsignatura_alm=request.POST["horarioAsignatura_alm"]
    descripcionAsignatura_alm=request.POST["descripcionAsignatura_alm"]

    #Insertando datos mediante el ORM de DJANGO
    asignaturaEditar=Asignatura.objects.get(idAsignatura_alm=idAsignatura_alm)
    asignaturaEditar.curso=cursoSeleccionado
    asignaturaEditar.nombreAsignatura_alm=nombreAsignatura_alm
    asignaturaEditar.creditosAsignatura_alm=creditosAsignatura_alm
    asignaturaEditar.fechaInicioAsignatura_alm=fechaInicioAsignatura_alm
    asignaturaEditar.fechaFinalizacionAsignatura_alm=fechaFinalizacionAsignatura_alm
    asignaturaEditar.profesorAsignatura_alm=profesorAsignatura_alm

    if silaboAsignatura_alm is not None:
        asignaturaEditar.silaboAsignatura_alm=silaboAsignatura_alm

    asignaturaEditar.horarioAsignatura_alm=horarioAsignatura_alm
    asignaturaEditar.descripcionAsignatura_alm=descripcionAsignatura_alm

    asignaturaEditar.save()
    messages.success(request,
      'Asignatura ACTUALIZADO Exitosamente')
    return redirect('/asignatura')
